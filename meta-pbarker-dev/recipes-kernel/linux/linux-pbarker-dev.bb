DESCRIPTION = "Linux kernel (pbarker.dev)"
SECTION = "kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

inherit kernel

# The ORC unwinder is enabled in x86_64_defconfig and needs libelf-dev
DEPENDS_append_x86-64 = " elfutils-native"

LINUX_PBARKER_DEV_BRANCH ?= "pbarker/dev"
LINUX_VMAJOR = "5"
LINUX_VMINOR = "9"
LINUX_VERSION = "${LINUX_VMAJOR}.${LINUX_VMINOR}"
PV = "${LINUX_VERSION}"
KERNEL_VERSION_SANITY_SKIP = "1"

SRC_URI = "git://gitlab.com/pbarker.dev/staging/linux.git;protocol=https;branch=${LINUX_PBARKER_DEV_BRANCH}"
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

python __anonymous() {
    # Handle checksums of older COPYING files
    vmajor = int(d.getVar("LINUX_VMAJOR"))
    vminor = int(d.getVar("LINUX_VMINOR"))
    if (vmajor, vminor) <= (4, 17):
        d.setVar("LIC_FILES_CHKSUM", "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7")
    elif (vmajor, vminor) <= (5, 5):
        d.setVar("LIC_FILES_CHKSUM", "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814")
}

KBUILD_DEFCONFIG_bbe = "multi_v7_defconfig"

do_configure_prepend() {
	if [ -n "${KBUILD_DEFCONFIG}" ] && [ -f "${S}/arch/${ARCH}/configs/${KBUILD_DEFCONFIG}" ]; then
		oe_runmake_call -C ${S} CC="${KERNEL_CC}" LD="${KERNEL_LD}" O=${B} ${KBUILD_DEFCONFIG}
	fi
}
